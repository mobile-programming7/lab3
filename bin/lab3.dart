import 'package:lab3/lab3.dart' as lab3;
import 'dart:io';
import 'dart:math';

void main() {
  String number = "8+4-(2*3)/2+2^2";
  print(number);
  print(sumNumber(orderOfEquation(number)));
}

List num(String s) {
  var ch = [];
  String char;
  for (int i = 0; i < s.length; i++) {
    char = s.substring(i, i + 1);
    ch.add(char);
  }

  return ch;
}

int oper(String c) {
  switch (c) {
    case "+":
    case "-":
      return 1;
    case "*":
    case "/":
      return 2;
    case "^":
      return 3;
  }
  return -1;
}

String orderOfEquation(String s) {
  String result = ("");

  var ch = [];
  var aa = num(s);

  for (int i = 0; i < s.length; ++i) {
    String c = aa[i];

    if (c.contains(new RegExp(r'[a-zA-Z0-9]'))) {
      result += c;
    } else if (c == "(") {
      ch.add("(");
    } else if (c == ")") {
      while (!ch.isEmpty && ch.last != "(") {
        result += ch.last;
        ch.removeLast();
      }
      ch.removeLast();
    } else {
      while (!ch.isEmpty && oper(c) <= oper(ch.last)) {
        result += ch.last;
        ch.removeLast();
      }
      ch.add(c);
    }
  }

  while (!ch.isEmpty) {
    result += ch.last;
    ch.removeLast();
  }

  return result;
}

double sumNumber(String s){

  var ch = [];
  var aa = num(s);

  for(int i=0; i<s.length; i++){
    String c = aa[i];
    if (c.contains(new RegExp(r'[a-zA-Z0-9]'))){  
      ch.add(double.parse(c));
    }else{
      double left = ch.removeLast();
      double right = ch.removeLast();

      switch(c){
        case "+":
        ch.add(right+left);
        break;
        
        case "-":
        ch.add(right-left);
        break;

        case "/":
        ch.add(right/left);
        break;

        case "*":
        ch.add(right*left);
        break;

        case "^":
        ch.add(pow(right, left));
        break;

      }
    }
  }
  return ch.removeLast();
}
