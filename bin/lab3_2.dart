import 'dart:io';

void main(List<String> args) {
  typemenu();
}

void typemenu(){
  print("Choose a type Menu");
  menu();
  String? choose = stdin.readLineSync()!;
  if (choose == '1' || choose == 'Coffee') {
    print("coffee");
    coffeeHI();
  }else if(choose == '2' || choose == 'Milk'){
    print("Milk");
    MilkHI();
  }else if(choose == '3' || choose == 'Tea'){
    print("Tea");
    TeaHI();
  }
}

void coffeeHI(){
  print("1.Hot / 2.Ice");
  String? chooseHI = stdin.readLineSync()!;
  if (chooseHI == '1') {
    hotCoffee();
    coffeeHot();
  }else if(chooseHI == '2'){
    iceCoffee();
    coffeeIce();
  }
}

void MilkHI(){
  print("1.Hot / 2.Ice");
  String? chooseHI = stdin.readLineSync()!;
  if (chooseHI == '1') {
    hotMilk();
    milkHot();
  }else if(chooseHI == '2'){
    iceMilk();
    milkIce();
  }
}

void TeaHI(){
  print("1.Hot / 2.Ice");
  String? chooseHI = stdin.readLineSync()!;
  if (chooseHI == '1') {
    hotTea();
    teaHot();
  }else if(chooseHI == '2'){
    iceTea();
    teaHot();
  }
}

void coffeeIce(){
   print("Choose a Menu");
  String? chooseMenu = stdin.readLineSync()!;
  if (chooseMenu == '1' || chooseMenu == '1.IceEspresso') {
    print("Menu: IceEspresso");
    print("price: 50");
  }else if(chooseMenu == '2' || chooseMenu == '2.IceAmerichna'){
    print("Menu: IceAmerichna");
    print("price: 50");
  }else if(chooseMenu == '3' || chooseMenu == '3.IceLatte'){
    print("Menu: IceLatte");
    print("price: 50");
  }
}

void coffeeHot(){
  print("Choose a Menu");
  String? chooseMenu = stdin.readLineSync()!;
  if (chooseMenu == '1' || chooseMenu == '1.HotEspresso') {
    print("Menu: HotEspresso");
    print("price: 45");
  }else if(chooseMenu == '2' || chooseMenu == '2.HotAmerichna'){
    print("Menu: HotAmerichna");
    print("price: 45");
  }else if(chooseMenu == '3' || chooseMenu == '3.HotLatte'){
    print("Menu: HotLatte");
    print("price: 45");
  }
}

void milkIce(){
   print("Choose a Menu");
  String? chooseMenu = stdin.readLineSync()!;
  if (chooseMenu == '1' || chooseMenu == '1.IceCaramelMilk') {
    print("Menu: IceEspresso");
    print("price: 50");
  }else if(chooseMenu == '2' || chooseMenu == '2.IceCocoa'){
    print("Menu: IceAmerichna");
    print("price: 50");
  }else if(chooseMenu == '3' || chooseMenu == '3.IceMilk'){
    print("Menu: IceMilk");
    print("price: 50");
  }
}

void milkHot(){
  print("Choose a Menu");
  String? chooseMenu = stdin.readLineSync()!;
  if (chooseMenu == '1' || chooseMenu == '1.HotCaramelMilk') {
    print("Menu: HotCaramelMilk");
    print("price: 45");
  }else if(chooseMenu == '2' || chooseMenu == '2.HotCocoa'){
    print("Menu: HotCocoa");
    print("price: 45");
  }else if(chooseMenu == '3' || chooseMenu == '3.HotMilk'){
    print("Menu: HotMilk");
    print("price: 45");
  }
}

void teaIce(){
  print("Choose a Menu");
  String? chooseMenu = stdin.readLineSync()!;
  if (chooseMenu == '1' || chooseMenu == '1.IceThaiMilk Tea') {
    print("Menu: IceThaiMilk Tea");
    print("price: 45");
  }else if(chooseMenu == '2' || chooseMenu == '2.IceBlack Tea'){
    print("Menu: IceBlack Tea");
    print("price: 45");
  }else if(chooseMenu == '3' || chooseMenu == '3.IceMatcha Tea'){
    print("Menu: IceMatcha Tea");
    print("price: 45");
  }
}

void teaHot(){
  print("Choose a Menu");
  String? chooseMenu = stdin.readLineSync()!;
  if (chooseMenu == '1' || chooseMenu == '1.HotThaiMilk Tea') {
    print("Menu: HotThaiMilk Tea");
    print("price: 45");
  }else if(chooseMenu == '2' || chooseMenu == '2.HotBlack Tea'){
    print("Menu: HotBlack Tea");
    print("price: 45");
  }else if(chooseMenu == '3' || chooseMenu == '3.HotMatcha Tea'){
    print("Menu: HotMatcha Tea");
    print("price: 45");
  }
}

void menu(){
  var tmenu = ["1.Coffee\n","2.Milk\n","3.Tea"];
  String menu = "";
  for(int i=0; i<tmenu.length; i++){
    menu += tmenu[i];
  }
  print(menu);
}

void hotCoffee() {
  var menuhot = ["1.HotEspresso\n","2.HotAmerichna\n","3.HotLatte"];
  String menu = "";
  for(int i=0; i<menuhot.length; i++){
     menu += menuhot[i];
  }
  print(menu);
}

void iceCoffee(){
  var menuice = ["1.IceEspresso\n","2.IceAmerichna\n","3.IceLatte"];
  String menu = "";
  for(int i=0; i<menuice.length; i++){
     menu += menuice[i];
  }
  print(menu);
}

void hotMilk(){
  var menuhot = ["1.HotCaramelMilk\n","2.HotCoca\n","3.HotMilk"];
  String menu = "";
  for(int i=0; i<menuhot.length; i++){
     menu += menuhot[i];
  }
  print(menu);
}

void iceMilk(){
  var menuice = ["1.IceCaramelMilk\n","2.IceCocoa\n","3.IceMilk"];
  String menu = "";
  for(int i=0; i<menuice.length; i++){
     menu += menuice[i];
  }
  print(menu);
}

void hotTea(){
  var menuhot = ["1.HotThaiMilk Tea\n","2.HotBlack Tea\n","3.HotMatcha Tea"];
  String menu = "";
  for(int i=0; i<menuhot.length; i++){
     menu += menuhot[i];
  }
  print(menu);
}

void iceTea(){
  var menuice = ["1.IceThaiMilk Tea\n","2.IceBlack Tea\n","3.IceMatcha Tea"];
  String menu = "";
  for(int i=0; i<menuice.length; i++){
     menu += menuice[i];
  }
  print(menu);
}